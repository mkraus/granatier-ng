project(granatier-ng)

# allow standalone build of Granatier (outside kdegames, e.g. in work branches)
if (NOT KDE4_FOUND)
  find_package(KDE4 REQUIRED)
  include(KDE4Defaults)
  include_directories(${KDE4_INCLUDES})
endif (NOT KDE4_FOUND)

find_package(LibKDEGames REQUIRED)
include_directories(${KDEGAMES_INCLUDE_DIRS})

add_subdirectory(themes)
add_subdirectory(arenas)
add_subdirectory(src)
