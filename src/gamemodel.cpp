/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Mathias Kraus <k.hias@gmx.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "gamemodel.h"


#include "arena.h"
#include "mapparser.h"


#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QKeyEvent>
#include <QFile>

#include <QXmlSimpleReader>

#include <KStandardDirs>
#include <KConfig>
#include <KComponentData>

#include <QTimer>

#include <QDebug>

#define CELLSIZE 100

typedef struct
{
    unsigned int rangeStart;
    unsigned int rangeEnd;
    void* prev;
    void* next;
} IdLinkedList;

static IdLinkedList* staticIdLinkedList = 0;

unsigned int getUniqueId()
{
    unsigned int nId = 0;
    bool bUniqueIdFound = false;
    
    if(staticIdLinkedList == 0)
    {
        staticIdLinkedList = new IdLinkedList;
        staticIdLinkedList->rangeStart = 0;
        staticIdLinkedList->rangeEnd = 0;
        staticIdLinkedList->prev = 0;
        staticIdLinkedList->next = 0;
        nId = 0;
        bUniqueIdFound = true;
    }
    
    if(!bUniqueIdFound && staticIdLinkedList->rangeStart != 0)
    {
        staticIdLinkedList->rangeStart -= 1;
        nId = staticIdLinkedList->rangeStart;
        bUniqueIdFound = true;
    }
    else if(!bUniqueIdFound)
    {
        staticIdLinkedList->rangeEnd += 1;
        nId = staticIdLinkedList->rangeEnd;
        bUniqueIdFound = true;
        //merge ranges if neccessary
        if(staticIdLinkedList->next != 0 && ((staticIdLinkedList->rangeEnd+1) == ((IdLinkedList*) staticIdLinkedList->next)->rangeStart))
        {
            IdLinkedList* temp = (IdLinkedList*) staticIdLinkedList->next;
            staticIdLinkedList->rangeEnd = temp->rangeEnd;
            staticIdLinkedList->next = temp->next;
            ((IdLinkedList*) staticIdLinkedList->next)->prev = temp->prev;
            delete temp;
        }
    }
    
    return nId;
}

void removeUniqueId(unsigned int nId)
{
    IdLinkedList* temp = staticIdLinkedList;
    //find range with the id
    while(temp != 0 && !(nId >= temp->rangeStart && nId <= temp->rangeEnd))
    {
        temp = (IdLinkedList*) temp->next;
    }
    
    if(temp != 0)
    {
        if(nId == temp->rangeStart)
        {
            temp->rangeStart += 1;
        }
        else if(nId == temp->rangeEnd)
        {
            temp->rangeEnd -= 1;
        }
        else
        {
            IdLinkedList* newIdRange = new IdLinkedList;
            newIdRange->rangeStart = nId + 1;
            newIdRange->rangeEnd = temp->rangeEnd;
            temp->rangeEnd = nId - 1;
            if(temp->next != 0)
            {
                ((IdLinkedList*) temp->next)->prev = newIdRange;
            }
            newIdRange->prev = temp;
            newIdRange->next = temp->next;
            temp->next = newIdRange;
        }
    }
}

void clearUniqueIds()
{
    while(staticIdLinkedList->next != 0)
    {
        IdLinkedList* temp = staticIdLinkedList;
        staticIdLinkedList = (IdLinkedList*) staticIdLinkedList->next;
        delete temp;
    }
}

GameModel::GameModel(QObject* parent): QGraphicsScene(parent)
{
    
    
    
    qRegisterMetaType<Granatier::SceneGraphUpdate>("Granatier::SceneGraphUpdate");
    
    
    
    
    
    // Create the Arena instance
    m_arena = new Arena();
    
    // Create the parser that will parse the XML file in order to initialize the Arena instance
    // This also creates all the characters
    MapParser mapParser(m_arena);
    // Set the XML file as input source for the parser
    QString filePath;
//     if(Settings::self()->randomArenaMode())
//     {
//         QStringList arenasAvailable;
//         KGlobal::dirs()->addResourceType("arenaselector", "data", KGlobal::mainComponent().componentName() + "/arenas/");
//         KGlobal::dirs()->findAllResources("arenaselector", "*.desktop", KStandardDirs::Recursive, arenasAvailable);
//         
//         qsrand(QDateTime::currentDateTime().toTime_t());
//         int nIndex = ((double) qrand() / RAND_MAX) * (arenasAvailable.count()-1);
//         if(nIndex < 0)
//         {
//             nIndex = 0;
//         }
//         else if(nIndex >= arenasAvailable.count())
//         {
//             nIndex = arenasAvailable.count() - 1;
//         }
//         filePath = KStandardDirs::locate("appdata", "arenas/" + arenasAvailable.at(nIndex));
//     }
//     else
//     {
//         filePath = KStandardDirs::locate("appdata", Settings::self()->arena());
//     }
    
    filePath = KStandardDirs::locate("appdata", "arenas/granatier.desktop");
    
    /*if(!QFile::exists(filePath))
    {
        Settings::self()->useDefaults(true);
        filePath = KStandardDirs::locate("appdata", Settings::self()->arena());
        Settings::self()->useDefaults(false);
    }*/
    
//     KConfig arenaConfig(filePath, KConfig::SimpleConfig);
//     KConfigGroup group = arenaConfig.group("Arena");
//     QString arenaFileName = group.readEntry("FileName");
    
    QFile arenaXmlFile(KStandardDirs::locate("appdata", "arenas/granatier.xml"));
    QXmlInputSource source(&arenaXmlFile);
    // Create the XML file reader
    QXmlSimpleReader reader;
    reader.setContentHandler(&mapParser);
    // Parse the XML file
    reader.parse(source);
    
//     QString arenaName = group.readEntry("Name");
//     m_arena->setName(arenaName);
    
    int nRows = m_arena->getNbRows();
    int nColumns = m_arena->getNbColumns();
    
    
    
    
    
    Granatier::Cell::Type cellType;
    Granatier::SceneGraphItem* sceneGraphItem;
    
    
    
    
    
    
    
    
    m_key = Qt::Key_No;
    m_keyEventSinceLastUpdate = false;
    
    QBrush wallBrush (QColor(64, 64, 64));
    QBrush groundBrush (QColor(192, 192, 128));
    QBrush blockBrush (QColor(192, 64, 0));
    QBrush holeBrush (QColor(0, 0, 0, 0));
    QBrush bombmortarBrush (QColor(160, 160, 32));
    QBrush iceBrush (QColor(192, 192, 255));
    QBrush arrowBrush (QColor(255, 255, 0));
    QBrush unknownBrush (QColor(255, 0, 0));
    QBrush playerBrush (QColor(255, 200, 0));
    QBrush* selectedBrush;
    
    setBackgroundBrush(QBrush(Qt::lightGray, Qt::Dense5Pattern));
    
    int zValue = 0;
    QGraphicsRectItem* groundSprite = 0;
    for(int i = 0; i < nRows; i++)
    {
        for(int j = 0; j < nColumns; j++)
        {
            groundSprite = new QGraphicsRectItem(j * CELLSIZE - CELLSIZE/2, i * CELLSIZE - CELLSIZE/2, CELLSIZE, CELLSIZE);
            cellType = m_arena->getCell(i, j).getType();
            switch(cellType)
            {
                case Granatier::Cell::WALL:
                    selectedBrush = &wallBrush;
                    zValue = 1;
                    break;
                case Granatier::Cell::GROUND:
                    selectedBrush = &groundBrush;
                    zValue = 0;
                    break;
                case Granatier::Cell::BLOCK:
                    selectedBrush = &blockBrush;
                    zValue = 1;
                    break;
                case Granatier::Cell::HOLE:
                    selectedBrush = &holeBrush;
                    zValue = 0;
                    break;
                case Granatier::Cell::BOMBMORTAR:
                    selectedBrush = &bombmortarBrush;
                    zValue = 0;
                    break;
                case Granatier::Cell::ICE:
                    selectedBrush = &iceBrush;
                    zValue = 0;
                    break;
                case Granatier::Cell::ARROWDOWN:
                case Granatier::Cell::ARROWUP:
                case Granatier::Cell::ARROWLEFT:
                case Granatier::Cell::ARROWRIGHT:
                    selectedBrush = &arrowBrush;
                    zValue = 0;
                    break;
                default:
                    selectedBrush = &unknownBrush;
                    zValue = 0;
            }
            groundSprite->setBrush(*selectedBrush);
            groundSprite->setZValue(0);
            m_groundSprites.append(groundSprite);
            addItem(groundSprite);
            
            sceneGraphItem = new Granatier::SceneGraphItem;
            sceneGraphItem->type = (Granatier::GraphicItem::Type) cellType;
            sceneGraphItem->id = getUniqueId();
            sceneGraphItem->x = j;
            sceneGraphItem->y = i;
            sceneGraphItem->zValue = zValue;
            m_sceneGraph.append(sceneGraphItem);
        }
    }
    
    m_player = new QGraphicsEllipseItem(-CELLSIZE*0.9/2, -CELLSIZE*0.9/2, CELLSIZE*0.9, CELLSIZE*0.9);
    m_player->setPos(0, 0);
    m_player->setBrush(playerBrush);
    m_player->setZValue(1);
    addItem(m_player);
    
    sceneGraphItem = new Granatier::SceneGraphItem;
    sceneGraphItem->type = Granatier::GraphicItem::PLAYER;
    sceneGraphItem->id = getUniqueId();
    sceneGraphItem->x = 0;
    sceneGraphItem->y = 0;
    sceneGraphItem->zValue = 2;
    m_sceneGraph.append(sceneGraphItem);
    
    QPolygonF polygon;
    polygon << QPointF(0, -CELLSIZE*0.9/2) << QPointF(0, CELLSIZE*0.9/2) << QPointF(0, 0);
    m_movingTriangle = new QGraphicsPolygonItem(polygon);
    m_movingTriangle->setPos(m_player->pos());
    m_movingTriangle->setZValue(2);
    addItem(m_movingTriangle);
    
    m_player2 = new QGraphicsEllipseItem(-CELLSIZE*0.9/2, -CELLSIZE*0.9/2, CELLSIZE*0.9, CELLSIZE*0.9);
    m_player2->setPos(0, 0);
    m_player2->setBrush(playerBrush);
    m_player2->setZValue(1);
    addItem(m_player2);
    
    sceneGraphItem = new Granatier::SceneGraphItem;
    sceneGraphItem->type = Granatier::GraphicItem::PLAYER;
    sceneGraphItem->id = getUniqueId();
    sceneGraphItem->x = 0;
    sceneGraphItem->y = 0;
    sceneGraphItem->zValue = 2;
    m_sceneGraph.append(sceneGraphItem);
    
    polygon << QPointF(0, -CELLSIZE*0.9/2) << QPointF(0, CELLSIZE*0.9/2) << QPointF(0, 0);
    m_movingTriangle2 = new QGraphicsPolygonItem(polygon);
    m_movingTriangle2->setPos(m_player2->pos());
    m_movingTriangle2->setZValue(2);
    addItem(m_movingTriangle2);
    
    // Start the Game timer
    m_timer = new QTimer(this);
    m_timer->setInterval(17);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
    m_timer->start();
    
    time.start();
}

GameModel::~GameModel()
{
    m_timer->stop();
    delete m_timer;
    
    QGraphicsRectItem* groundSprite;
    
    QList<QGraphicsRectItem*>::const_iterator i;
    while(!m_groundSprites.isEmpty())
    {
        groundSprite = m_groundSprites.takeFirst();
        removeItem(groundSprite);
        delete groundSprite;
    }
    
    removeItem(m_movingTriangle);
    delete m_movingTriangle;
    
    removeItem(m_player);
    delete m_player;
    
    removeItem(m_movingTriangle2);
    delete m_movingTriangle2;
    
    removeItem(m_player2);
    delete m_player2;
    
    while(!m_sceneGraph.isEmpty())
    {
        delete m_sceneGraph.takeFirst();
    }
    
    clearUniqueIds();
}

static int pressCounter = 0;
void GameModel::keyPressEvent(int key)
{
    m_key = QKeySequence(key);
    m_timer->start();
    m_keyEventSinceLastUpdate = true;
}

static int releaseCounter = 0;
void GameModel::keyReleaseEvent(int key)
{
    if(QKeySequence(key) == m_key)
    {
        m_key = Qt::Key_No;
        m_timer->stop();
    }
    m_keyEventSinceLastUpdate = true;
}

void GameModel::pauseGame()
{
    m_timer->stop();
    m_key = Qt::Key_No;
    m_keyEventSinceLastUpdate = false;
}

Granatier::SceneGraph GameModel::getSceneGraph()
{
    return m_sceneGraph;
}

void GameModel::update()
{
    Granatier::SceneGraphUpdate sceneGraphUpdate;
    
    QPolygonF tempPolygon;
    if(m_keyEventSinceLastUpdate)
    {
        tempPolygon = m_movingTriangle->polygon();
        tempPolygon.replace(2, QPointF(0, 0));
        m_movingTriangle->setPolygon(tempPolygon);
        tempPolygon = m_movingTriangle2->polygon();
        tempPolygon.replace(2, QPointF(0, 0));
        m_movingTriangle2->setPolygon(tempPolygon);
    }
    
    switch(m_key)
    {
        case Qt::Key_Up:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle->setRotation(-90);
                tempPolygon = m_movingTriangle->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/10., 0));
                m_movingTriangle->setPolygon(tempPolygon);
            }
            m_player->setPos(m_player->pos().x(), m_player->pos().y() - CELLSIZE/10.);
            break;
        case Qt::Key_Down:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle->setRotation(90);
                tempPolygon = m_movingTriangle->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/20., 0));
                m_movingTriangle->setPolygon(tempPolygon);
            }
            m_player->setPos(m_player->pos().x(), m_player->pos().y() + CELLSIZE/20.);
            break;
        case Qt::Key_Left:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle->setRotation(180);
                tempPolygon = m_movingTriangle->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/40., 0));
                m_movingTriangle->setPolygon(tempPolygon);
            }
            m_player->setPos(m_player->pos().x() - CELLSIZE/40., m_player->pos().y());
            break;
        case Qt::Key_Right:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle->setRotation(0);
                tempPolygon = m_movingTriangle->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/20., 0));
                m_movingTriangle->setPolygon(tempPolygon);
            }
            m_player->setPos(m_player->pos().x() + CELLSIZE/20., m_player->pos().y());
            break;
    }
    m_movingTriangle->setPos(m_player->pos());
    
    switch(m_key)
    {
        case Qt::Key_W:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle2->setRotation(-90);
                tempPolygon = m_movingTriangle2->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/10., 0));
                m_movingTriangle2->setPolygon(tempPolygon);
            }
            m_player2->setPos(m_player2->pos().x(), m_player2->pos().y() - CELLSIZE/10.);
            break;
        case Qt::Key_S:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle2->setRotation(90);
                tempPolygon = m_movingTriangle2->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/20., 0));
                m_movingTriangle2->setPolygon(tempPolygon);
            }
            m_player2->setPos(m_player2->pos().x(), m_player2->pos().y() + CELLSIZE/20.);
            break;
        case Qt::Key_A:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle2->setRotation(180);
                tempPolygon = m_movingTriangle2->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/40., 0));
                m_movingTriangle2->setPolygon(tempPolygon);
            }
            m_player2->setPos(m_player2->pos().x() - CELLSIZE/40., m_player2->pos().y());
            break;
        case Qt::Key_D:
            if(m_keyEventSinceLastUpdate)
            {
                m_movingTriangle2->setRotation(0);
                tempPolygon = m_movingTriangle2->polygon();
                tempPolygon.replace(2, QPointF(20*CELLSIZE/20., 0));
                m_movingTriangle2->setPolygon(tempPolygon);
            }
            m_player2->setPos(m_player2->pos().x() + CELLSIZE/20., m_player2->pos().y());
            break;
    }
    m_movingTriangle2->setPos(m_player2->pos());
    
    if(m_keyEventSinceLastUpdate)
    {
        m_keyEventSinceLastUpdate = false;
    }
    
    Granatier::SceneGraphItem* sceneGraphItem = m_sceneGraph.at(m_sceneGraph.size() - 2);
    Granatier::SceneGraphUpdateItem* sceneGraphUpdateItem = new Granatier::SceneGraphUpdateItem;
    sceneGraphUpdateItem->type = sceneGraphItem->type;
    sceneGraphUpdateItem->id = sceneGraphItem->id;
    sceneGraphUpdateItem->x = (int) m_player->pos().x();
    sceneGraphUpdateItem->y = (int) m_player->pos().y();
    sceneGraphUpdateItem->zValue = sceneGraphItem->zValue;
    sceneGraphUpdate.append(sceneGraphUpdateItem);
    
    sceneGraphItem = m_sceneGraph.at(m_sceneGraph.size() - 1);
    sceneGraphUpdateItem = new Granatier::SceneGraphUpdateItem;
    sceneGraphUpdateItem->type = sceneGraphItem->type;
    sceneGraphUpdateItem->id = sceneGraphItem->id;
    sceneGraphUpdateItem->x = (int) m_player2->pos().x();
    sceneGraphUpdateItem->y = (int) m_player2->pos().y();
    sceneGraphUpdateItem->zValue = sceneGraphItem->zValue;
    sceneGraphUpdate.append(sceneGraphUpdateItem);
    
    //instead of emitting this signal, start a thread in GameScene to do the update
    emit hasUpdate(sceneGraphUpdate);
}