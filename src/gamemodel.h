/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Mathias Kraus <k.hias@gmx.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include "granatierglobals.h"

#include <QGraphicsScene>
#include <QKeySequence>

#include <QList>
#include <QPair>


#include <QTime>

class QGraphicsRectItem;
class QGraphicsEllipseItem;
class QGraphicsPolygonItem;
class QTimer;

class Arena;

class GameModel : public QGraphicsScene
{   
    Q_OBJECT
    
public:
    GameModel(QObject* parent = 0);
    
    virtual ~GameModel();
    
    Granatier::SceneGraph getSceneGraph();
    
    
    
private:
    QList<QGraphicsRectItem*> m_groundSprites;
    QGraphicsEllipseItem* m_player;
    QGraphicsPolygonItem* m_movingTriangle;
    
    QGraphicsEllipseItem* m_player2;
    QGraphicsPolygonItem* m_movingTriangle2;
    
    QKeySequence m_key;
    
    QTimer* m_timer;
    
    bool m_keyEventSinceLastUpdate;
    
    QTime time;
    
    Granatier::SceneGraph m_sceneGraph;
    
    Arena* m_arena;
    
private slots:
    void update();
    
public slots:
    /**
     * Manages the key press events.
     * @param key the key pressed
     */
    void keyPressEvent(int key);
    
    /**
     * Manages the key release events.
     * @param key the key released
     */
    void keyReleaseEvent(int key);
    
    void pauseGame();
    
signals:
    void hasUpdate(Granatier::SceneGraphUpdate sceneGraphUpdate);

};

#endif // GAMEMODEL_H
