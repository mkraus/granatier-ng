/*
 *  <one line to give the program's name and a brief idea of what it does.>
 *  Copyright (C) 2011  Mathias Kraus <k.hias@gmx.de>
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef GRANATIERGLOBALS_H
#define GRANATIERGLOBALS_H

#include <KGameRenderedItem>

#include <QList>

namespace Granatier
{
    namespace GraphicItem
    {
        enum Type
        {
            NONE,
            HOLE,
            GROUND,
            WALL,
            BLOCK,
            ICE,
            BOMBMORTAR,
            ARROWUP,
            ARROWDOWN,
            ARROWLEFT,
            ARROWRIGHT,
            PLAYER,
            BOMB,
            BONUS,
            SCORE
        };
    }
    
    /** The Cell possible types */
    namespace Cell
    {
        enum Type
        {
            HOLE            = GraphicItem::HOLE,
            GROUND          = GraphicItem::GROUND,
            WALL            = GraphicItem::WALL,
            BLOCK           = GraphicItem::BLOCK,
            ICE             = GraphicItem::ICE,
            BOMBMORTAR      = GraphicItem::BOMBMORTAR,
            ARROWUP         = GraphicItem::ARROWUP,
            ARROWDOWN       = GraphicItem::ARROWDOWN,
            ARROWLEFT       = GraphicItem::ARROWLEFT,
            ARROWRIGHT      = GraphicItem::ARROWRIGHT
        };
    }
    
    /** The Element possible types */
    namespace Element
    {
        enum Type
        {
            NONE            = GraphicItem::NONE,
            BLOCK           = GraphicItem::BLOCK,
            PLAYER          = GraphicItem::PLAYER,
            BOMB            = GraphicItem::BOMB,
            BONUS           = GraphicItem::BONUS,
            SCORE           = GraphicItem::SCORE    //TODO: check if used
        };
    }
    
    namespace Direction
    {
        enum Direction
        {
            NORTH,
            SOUTH,
            EAST,
            WEST
        };
    }
    
    typedef struct
    {
        GraphicItem::Type   type;
        unsigned int        id;
        int                 x;
        int                 y;
        int                 zValue;
    } SceneGraphItem;
    
    typedef QList<SceneGraphItem*> SceneGraph;
    
    typedef struct
    {
        GraphicItem::Type   type;
        unsigned int        id;
        int                 x;
        int                 y;
        int                 zValue;
    } SceneGraphUpdateItem;
    
    typedef QList<SceneGraphUpdateItem*> SceneGraphUpdate;
    
    typedef struct
    {
        GraphicItem::Type   type;
        KGameRenderedItem*  sprite;
        unsigned int        id;
        int                 x;
        int                 y;
        int                 zValue;
    } SceneGraphSpriteItem;
    
    typedef QList<SceneGraphSpriteItem*> SceneGraphSprites;
}

#endif // GRANATIERGLOBALS_H
