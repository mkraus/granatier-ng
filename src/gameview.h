/*
 * Copyright 2009 Mathias Kraus <k.hias@gmx.de>
 * Copyright 2007-2008 Thomas Gallinari <tg8187@yahoo.fr>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include <QGraphicsView>
#include <QTimer>

class QKeyEvent;
class GameScene;

/**
 * @brief This class manages the drawing of each element of the Game instance.
 * It creates a GameScene instance associated to the given Game instance in order to manage the elements to be drawn at each moment of the game.
 */
class GameView : public QGraphicsView {

Q_OBJECT

public:

    /**
      * Creates a new GameView instance.
      * @param game the Game instance whose elements have to be drawn
      */
    //GameView(Game* game);
    GameView(GameScene* scene);

    /**
      * Deletes the GameView instance.
      */
    ~GameView();

    /**
      * Resizes the items when the view is resized.
      * @param event the resize event
      */
    void resizeEvent(QResizeEvent* event);

protected:
    /**
     * Manages the player actions by hanlding the key press events.
     * @param keyEvent the key press event
     */
    void keyPressEvent(QKeyEvent* keyEvent);
    
    /**
     * Manages the player actions by hanlding the key release events.
     * @param keyEvent the key release event
     */
    void keyReleaseEvent(QKeyEvent* keyEvent);
    
    void focusOutEvent(QFocusEvent*);

    
private:

    
private slots:


signals:
    void sizeChanged(QSize newSize);
    
    /**
     * Emitted on key press event for the Game instance
     * @param key the key pressed
     */
    void keyPressed(int key);
    
    /**
     * Emitted on key release event for the Game instance
     * @param key the key released
     */
    void keyReleased(int key);
    
    void lostFocus();
};

#endif

