/*
 * Copyright 2009 Mathias Kraus <k.hias@gmx.de>
 * Copyright 2007-2008 Thomas Gallinari <tg8187@yahoo.fr>
 * Copyright 2007-2008 Alexandre Galinier <alex.galinier@hotmail.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMESCENE_H
#define GAMESCENE_H

#include "granatierglobals.h"

#include <QGraphicsScene>

#include <QList>
#include <QHash>
#include <QMap>

#include "gamemodel.h"

class KGameRenderer;
class KGameRenderedItem;
class QGraphicsRectItem;
class QTimer;
class GameModel;

/**
 * @brief This class contains all the Game elements to be drawn on the screen by the GameView instance.
 */
class GameScene : public QGraphicsScene {

Q_OBJECT
    
private:
    /** The KGameRenderer */
    KGameRenderer* m_rendererSelectedTheme;
    KGameRenderer* m_rendererDefaultTheme;
    
    Granatier::SceneGraphSprites m_sceneGraphSprites;
    KGameRenderedItem* m_backgroundSprite;
    
    QTimer* m_timer;
    
    GameModel* m_gameModel;
    
    int m_nRows;
    int m_nColumns;
    
    qreal m_scalingFactor;

    
public:

    /**
      * Creates a new GameScene instance.
      * @param p_game the Game instance whose elements must be contained in the GameScene in order to be drawn
      */
    GameScene();
    
    /**
      * Deletes the Game instance.
      */
    ~GameScene();

    /**
     * Initializes class
     */
    void init();
    
    /**
     * Cleans class
     */
    void cleanUp();

    void setGameModel(GameModel* gameModel);
    
private slots:
    void doLayout();
public slots:    
    void update(Granatier::SceneGraphUpdate sceneGraphUpdate);
    
signals:

};

#endif

