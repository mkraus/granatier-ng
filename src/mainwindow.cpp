/*
 * Copyright 2008 Mathias Kraus <k.hias@gmx.de>
 * Copyright 2007-2008 Thomas Gallinari <tg8187@yahoo.fr>
 * Copyright 2007-2008 Pierre-Benoit Bessse <besse@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "gameview.h"
#include "gamescene.h"
#include "gamemodel.h"

#include <QGraphicsView>
#include <QTimer>
#include <QThread>
#include <KActionCollection>
#include <KStandardGameAction>
#include <KToggleAction>
#include <KMessageBox>
#include <KConfigDialog>
#include <KGameThemeSelector>
#include <KLocale>

#include <QGraphicsSvgItem>
#include <QDockWidget>

#include <QDebug>
//#define GRANATIER_SHOW_DEBUG_VIEW
MainWindow::MainWindow()
{
    // Initialize the game
    m_gameScene = NULL;
    m_gameModel = NULL;
    m_view = NULL;
    m_modelThread = NULL;
    
    m_modelDebugView = NULL;
    m_modelDebugWidget = NULL;
    
    // Set the window menus
    KStandardGameAction::gameNew(this, SLOT(newGame(bool)), actionCollection());
    KStandardGameAction::quit(this, SLOT(close()), actionCollection());
    
    // init game
    initGame();
    // Setup the window
    setupGUI(Keys | Save | Create);
}

MainWindow::~MainWindow()
{
    delete m_view;
    delete m_gameScene;
    if(m_modelThread)
    {
        m_modelThread->terminate();
        m_modelThread->wait();
    }
    delete m_gameModel;
    delete m_modelThread;
}

void MainWindow::initGame()
{
    delete m_view;
    delete m_gameScene;
    if(m_modelThread)
    {
        m_modelThread->terminate();
        m_modelThread->wait();
    }
    delete m_gameModel;
    delete m_modelThread;
    
    if(m_modelDebugWidget && dockWidgetArea(m_modelDebugWidget)!=Qt::NoDockWidgetArea)
    {
        removeDockWidget(m_modelDebugWidget);
    }
    delete m_modelDebugView;
    delete m_modelDebugWidget;
    
    m_modelThread = new QThread();
    m_gameModel = new GameModel();
    m_gameModel->moveToThread(m_modelThread);
    
#ifdef GRANATIER_SHOW_DEBUG_VIEW
    m_modelDebugWidget = new QDockWidget(this);
    m_modelDebugView = new QGraphicsView(m_gameModel);
    m_modelDebugWidget->setWidget(m_modelDebugView);
    //m_modelDebugView->fitInView(m_gameModel->sceneRect(), Qt::KeepAspectRatio);
    m_modelDebugView->scale(0.2, 0.2);
    addDockWidget(Qt::LeftDockWidgetArea, m_modelDebugWidget);
#endif
    
    m_gameScene = new GameScene();
    
    // Create a new GameView instance
    m_view = new GameView(m_gameScene);
    m_view->setBackgroundBrush(Qt::black);
    setCentralWidget(m_view);
    m_gameScene->setGameModel(m_gameModel);
    
    connect(m_view, SIGNAL(keyPressed(int)), m_gameModel, SLOT(keyPressEvent(int)));
    connect(m_view, SIGNAL(keyReleased(int)), m_gameModel, SLOT(keyReleaseEvent(int)));
    connect(m_view, SIGNAL(lostFocus()), m_gameModel, SLOT(pauseGame()));
    
    m_modelThread->start();
    
    this->setFocus();
    this->setFocusProxy(m_view);
}

void MainWindow::newGame(const bool gameOver)
{

    // Start a new game
    initGame();
}

void MainWindow::close()
{
    // Confirm before closing
    if(KMessageBox::warningYesNo(this, i18n("Are you sure you want to quit?"), i18nc("To quit Granatier", "Quit")) == KMessageBox::Yes)
    {
        KXmlGuiWindow::close();
    }

}

