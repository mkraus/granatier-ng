/*
 * Copyright 2009 Mathias Kraus <k.hias@gmx.de>
 * Copyright 2007-2008 Thomas Gallinari <tg8187@yahoo.fr>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gameview.h"
#include "gamescene.h"

#include <QKeyEvent>

GameView::GameView(GameScene* scene) : QGraphicsView(scene)
{
    setFrameShape(QFrame::NoFrame);
    setFocusPolicy(Qt::StrongFocus);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    
    setCacheMode(QGraphicsView::CacheBackground);
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    
    setOptimizationFlags( QGraphicsView::DontClipPainter | QGraphicsView::DontSavePainterState );
    
    connect(this, SIGNAL(sizeChanged(QSize)), this->scene(), SLOT(doLayout()));
}

GameView::~GameView()
{

}

void GameView::resizeEvent(QResizeEvent* event)
{
    QGraphicsView::resizeEvent(event);
    emit sizeChanged(event->size());
}

void GameView::keyPressEvent(QKeyEvent* keyEvent)
{
    if(!keyEvent->isAutoRepeat())
    {
        emit keyPressed(keyEvent->key());
    }
}

void GameView::keyReleaseEvent(QKeyEvent* keyEvent)
{
    if(!keyEvent->isAutoRepeat())
    {
        emit keyReleased(keyEvent->key());
    }
}

void GameView::focusOutEvent(QFocusEvent*)
{
    emit lostFocus();
}
