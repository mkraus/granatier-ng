/*
 * Copyright 2009 Mathias Kraus <k.hias@gmx.de>
 * Copyright 2007-2008 Thomas Gallinari <tg8187@yahoo.fr>
 * Copyright 2007-2008 Alexandre Galinier <alex.galinier@hotmail.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamescene.h"
#include "gamemodel.h"


#include <KGameTheme>
#include <KLocale>
#include <QPainter>
#include <QGraphicsView>
#include <QGraphicsRectItem>

#include <KGameRenderer>
#include <KGameRenderedItem>

#include <QTimer>

#include <QDebug>

#define SIZE 100

GameScene::GameScene()
{
    m_gameModel = 0;
    m_sceneGraphSprites.clear();
    m_backgroundSprite = 0;
    m_nRows = 0;
    m_nColumns = 0;
    
    m_scalingFactor = 1;
    
    m_rendererDefaultTheme = new KGameRenderer("themes/granatier.test.desktop");
    m_rendererSelectedTheme = m_rendererDefaultTheme;
    
    m_backgroundSprite = new KGameRenderedItem(m_rendererDefaultTheme, "background");
    m_backgroundSprite->setRenderSize(QSize(1, 1));
    m_backgroundSprite->setScale(1);
    m_backgroundSprite->setZValue(0);
    addItem(m_backgroundSprite);

    init();
    
    doLayout();
    
    // Start the Game timer
    m_timer = new QTimer(this);
    m_timer->setInterval(25);
    //connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
    //m_timer->start();
}

void GameScene::init()
{

}

GameScene::~GameScene()
{
    m_timer->stop();
    delete m_timer;
    cleanUp();
    
    Granatier::SceneGraphSpriteItem* sceneGraphSpriteItem;
    while(!m_sceneGraphSprites.isEmpty())
    {
        sceneGraphSpriteItem = m_sceneGraphSprites.takeFirst();
        removeItem(sceneGraphSpriteItem->sprite);
        delete sceneGraphSpriteItem->sprite;
        delete sceneGraphSpriteItem;
    }
    
    if(m_rendererDefaultTheme == m_rendererSelectedTheme)
    {
        delete m_rendererDefaultTheme;
    }
    else
    {
        delete m_rendererDefaultTheme;
        delete m_rendererSelectedTheme;
    }
}

void GameScene::cleanUp()
{
}

void GameScene::setGameModel(GameModel* gameModel)
{
    m_gameModel = gameModel;
    connect(m_gameModel, SIGNAL(hasUpdate(Granatier::SceneGraphUpdate)), this, SLOT(update(Granatier::SceneGraphUpdate)));
    Granatier::SceneGraph sceneGraph = m_gameModel->getSceneGraph();
    
    Granatier::SceneGraphSpriteItem* sceneGraphSpriteItem;
    m_nRows = 0;
    m_nColumns = 0;
    
    KGameRenderedItem* sprite = 0;
    QString spriteKey;
    Granatier::SceneGraphItem* sceneGraphItem;
    foreach(sceneGraphItem, sceneGraph)
    {
        switch(sceneGraphItem->type)
        {
            case Granatier::GraphicItem::WALL:
                spriteKey = "arena_wall";
                break;
            case Granatier::GraphicItem::GROUND:
                spriteKey = "arena_ground";
                break;
            case Granatier::GraphicItem::BLOCK:
                spriteKey = "arena_block";
                break;
            case Granatier::GraphicItem::BOMBMORTAR:
                spriteKey = "arena_bomb_mortar";
                break;
            case Granatier::GraphicItem::ICE:
                spriteKey = "arena_ice";
                break;
            case Granatier::GraphicItem::ARROWDOWN:
                spriteKey = "arena_arrow_down";
                break;
            case Granatier::GraphicItem::ARROWUP:
                spriteKey = "arena_arrow_up";
                break;
            case Granatier::GraphicItem::ARROWLEFT:
                spriteKey = "arena_arrow_left";
                break;
            case Granatier::GraphicItem::ARROWRIGHT:
                spriteKey = "arena_arrow_right";
                break;
            default:
                spriteKey = "unknown";
        }
        
        sceneGraphSpriteItem = new Granatier::SceneGraphSpriteItem;
        sceneGraphSpriteItem->sprite = 0;
        sceneGraphSpriteItem->type = sceneGraphItem->type;
        sceneGraphSpriteItem->id = sceneGraphItem->id;
        sceneGraphSpriteItem->x = sceneGraphItem->x;
        sceneGraphSpriteItem->y = sceneGraphItem->y;
        sceneGraphSpriteItem->zValue = sceneGraphItem->zValue;
        
        if(sceneGraphItem->type != Granatier::GraphicItem::HOLE)
        {
            sprite = new KGameRenderedItem(m_rendererDefaultTheme, spriteKey);
            sprite->setRenderSize(QSize(1, 1));
            sprite->setScale(1);
            sprite->setZValue(sceneGraphSpriteItem->zValue);
            sceneGraphSpriteItem->sprite = sprite;
            m_sceneGraphSprites.append(sceneGraphSpriteItem);
            addItem(sprite);
        }
        if(sceneGraphItem->y > m_nRows)
        {
            m_nRows = sceneGraphItem->y;
        }
        if(sceneGraphItem->x > m_nColumns)
        {
            m_nColumns = sceneGraphItem->x;
        }
    }
    
    m_nRows += 1;
    m_nColumns += 1;
    
    doLayout();
}


void GameScene::doLayout()
{
    if(views().isEmpty())
    {
        return;
    }
    
    if(m_sceneGraphSprites.isEmpty())
    {
        return;
    }
    
    QSize size = views().first()->size();
    double height = views().first()->size().height();
    double width = views().first()->size().width();
    
    if(height/m_nRows < width/m_nColumns)
    {
        m_scalingFactor = (height/m_nRows)/SIZE;
    }
    else
    {
        m_scalingFactor = (width/m_nColumns)/SIZE;
    }
    
    int edgeLength = SIZE * m_scalingFactor;
    
    //the edge length has to fit into whole pixel, therefore the scaling factor has also discrete values
    m_scalingFactor = ((qreal) edgeLength) / SIZE;
    
    Granatier::SceneGraphSpriteItem* sceneGraphSpriteItem;
    foreach(sceneGraphSpriteItem, m_sceneGraphSprites)
    {
        sceneGraphSpriteItem->sprite->setRenderSize(QSize(edgeLength, edgeLength));
        if(sceneGraphSpriteItem->type == Granatier::GraphicItem::PLAYER)
        {
            sceneGraphSpriteItem->sprite->setPos(m_scalingFactor * sceneGraphSpriteItem->x, m_scalingFactor * sceneGraphSpriteItem->y);
        }
        else
        {
            sceneGraphSpriteItem->sprite->setPos(edgeLength * sceneGraphSpriteItem->x, edgeLength * sceneGraphSpriteItem->y);
        }
    }
    
    setSceneRect(QRectF(-edgeLength, -edgeLength, (m_nColumns+2) * edgeLength, (m_nRows+2) * edgeLength));
    
    views().first()->centerOn(sceneRect().center());
    
    QRect viewGeometry = views().first()->rect();
    QPointF sceneTopLeft = views().first()->mapToScene(viewGeometry.topLeft());
    QPointF sceneBottomRight = views().first()->mapToScene(viewGeometry.bottomRight());
    int sceneWidth = sceneBottomRight.x() - sceneTopLeft.x();
    int sceneHeight = sceneBottomRight.y() - sceneTopLeft.y();
 
    m_backgroundSprite->setRenderSize(QSize(sceneWidth+1, sceneHeight+1));
    m_backgroundSprite->setPos(sceneTopLeft);
}

void GameScene::update(Granatier::SceneGraphUpdate sceneGraphUpdate)
{
    if(m_gameModel == 0)
    {
        return;
    }
    
    while(!sceneGraphUpdate.isEmpty())
    {
        Granatier::SceneGraphUpdateItem* sceneGraphUpdateItem = sceneGraphUpdate.takeFirst();
        Granatier::SceneGraphSpriteItem* sceneGraphSpriteItem;
        for(int i = 0; i < m_sceneGraphSprites.size(); i++)
        {
            sceneGraphSpriteItem = m_sceneGraphSprites.at(i);
            if(sceneGraphSpriteItem->id == sceneGraphUpdateItem->id)
            {
                sceneGraphSpriteItem = m_sceneGraphSprites.at(i);
                sceneGraphSpriteItem->x = sceneGraphUpdateItem->x;
                sceneGraphSpriteItem->y = sceneGraphUpdateItem->y;
                sceneGraphSpriteItem->sprite->setPos(m_scalingFactor * sceneGraphSpriteItem->x, m_scalingFactor * sceneGraphSpriteItem->y);
            
                break;
            }
        }
        
        delete sceneGraphUpdateItem;
    }    
}
